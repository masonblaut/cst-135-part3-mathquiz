package com.example.mathquiz;

import java.util.Random;

public class Question {

    private int firstNum;
    private int secondNum;

    private int answer;

    private int [] answerArray ;
    private int answerPosition;

    private int upperLimit;

    private String questionPhrase;

    //generate new question
    public Question(int upperLimit){
        this.upperLimit = upperLimit;
        Random r = new Random();

        this.firstNum = r.nextInt(upperLimit);
        this.secondNum = r.nextInt(upperLimit);
        this.answer = this.firstNum+secondNum;
        this.questionPhrase = firstNum + " + "+secondNum+" = ";

        this.answerPosition = r.nextInt(4);
        this.answerArray = new int[] {0, 1, 2, 3};

        this.answerArray[0] = answer +1;
        this.answerArray[1] = answer +10;
        this.answerArray[2] = answer -5;
        this.answerArray[3] = answer -2;

        this.shuffleArray(this.answerArray);

        answerArray[answerPosition] = answer;

    }

    private int [] shuffleArray(int[] intArr){
        int index, temp;
        Random r = new Random();

        for(int i = intArr.length-1; i>0; i-- ){
            index = r.nextInt(i+1);
            temp = intArr[index];
            intArr[index] = intArr[i];
            intArr[i] = temp;
        }
        return intArr;
    }

    public int getFirstNum() {
        return firstNum;
    }

    public void setFirstNum(int firstNum) {
        this.firstNum = firstNum;
    }

    public int getSecondNum() {
        return secondNum;
    }

    public void setSecondNum(int secondNum) {
        this.secondNum = secondNum;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public int[] getAnswerArray() {
        return answerArray;
    }

    public void setAnswerArray(int[] answerArray) {
        this.answerArray = answerArray;
    }

    public int getAnswerPosition() {
        return answerPosition;
    }

    public void setAnswerPosition(int answerPosition) {
        this.answerPosition = answerPosition;
    }

    public int getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(int upperLimit) {
        this.upperLimit = upperLimit;
    }

    public String getQuestionPhrase() {
        return questionPhrase;
    }

    public void setQuestionPhrase(String questionPhrase) {
        this.questionPhrase = questionPhrase;
    }


}
